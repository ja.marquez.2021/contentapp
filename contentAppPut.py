import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""
PAGE_PUT = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource {resource} puted.</p>
  </body>
</html>
"""
# Aqui lo que estamos haciendo es crear una clase llamada ContentApp la cual herede de webApp, donde pasamos el port y la ip
# para que ellos "creen" en servidor y lo que definimos auqi son funciones para este.

class ContentAppPut(webapp.webApp):
    # diccionario de contenidos a servir
    def __init__(self,hostname,port):
        self.contents ={'/': "<p>Main page</p>",
                '/hello': "<p>Hello, people</p>",
                '/bye': "<p>Bye all!!</p>"}
        super().__init__(hostname,port)
    # sobreescribimos parse de webApp para que nos analice lo que queremos, en este caso el nombre del recurso (que
    # esta en la segunda posicion de la request si lo separamos por espacios)

    def parse (self, request):
        result = {"recurso": "", "contenido": "", "metodo": ""}
        result["recurso"] = request.split(' ')[1]
        result["contenido"] = request.split("\n")[-1]
        result["metodo"] = request.split(' ')[0]
        print(result)

        return result

    #sobreescribimos el process, en este caso para que cuendo el cliente pida un recurso, este le pase una pagina la
    # cual muestre en pantalla el valor que este bindeado con el nombre del recurso, si no tenemos el nombre del recurso
    # en la lista, le decimos 404 ; la forma de return es (codigo de respuesta, la pagina que quiero devolver)
    def process (self, resource):
        if (resource["metodo"] == "GET"):
            if resource["recurso"] in self.contents:
                content = self.contents[resource["recurso"]]
                page = PAGE.format(content=content)
                code = "200 OK"
            else:
                page = PAGE_NOT_FOUND.format(resource=resource["recurso"])
                code = "404 Resource Not Found"
            return (code, page)
        elif (resource["metodo"] == "PUT"):
            self.contents.update({resource["recurso"]: resource["contenido"]})
            page = PAGE_PUT.format(resource=resource["contenido"])
            code = "200 OK"
            print(self.contents)
            return (code, page)


if __name__ == "__main__":
    #PREGUNTA --> al estar ContentApp heredando webapp, cuando hay funcionen sobreescritas, pese a que en el init de
    # webapp llame esas funciones, las que realmente se ejecutaran son las de la ultima capa?   (parece que si)
    webApp = ContentAppPut ("localhost", 1238)